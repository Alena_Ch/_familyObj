public class Family {
    public static void main(String[] args) {

        FamilyMember memberOne = new FamilyMember("Иван", "мужской", 35, "отец Насти, Кати и Андрея");
        memberOne.getInformation();

        FamilyMember memberTwo = new FamilyMember("Анна", "женский", 33, "мать Насти, Кати и Андрея");
        memberTwo.getInformation();

        FamilyMember memberThree = new FamilyMember("Настя", "женский", 10, "дочь Ивана и Аннв");
        memberThree.getInformation();

        FamilyMember memberFour = new FamilyMember("Катя", "женский", 5, "дочь Ивана и Аннв");
        memberFour.getInformation();

        FamilyMember memberFive = new FamilyMember("Андрей", "мужской", 2, "сын Ивана и Аннв");
        memberFive.getInformation();

        FamilyMember memberSix = new FamilyMember("Евгений", "мужской", 60, "отец Ивана и дедушка Насти, Кати и Андрея");
        memberSix.getInformation();

        FamilyMember memberSeven = new FamilyMember("Ольга", "женский", 59, "мать Ивана и бабушка Насти, Кати и Андрея");
        memberSeven.getInformation();

        FamilyMember memberEight = new FamilyMember("Александр", "мужской", 61, "отец Анны и дедушка Насти, Кати и Андрея");
        memberEight.getInformation();

        FamilyMember memberNine = new FamilyMember("Елена", "женский", 58, "мать Анны и бабушка Насти, Кати и Андрея");
        memberNine.getInformation();
    }
}
