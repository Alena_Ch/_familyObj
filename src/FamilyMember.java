public class FamilyMember {
    private String name;
    private String gender;
    private int age;
    private String familyRole;

        public FamilyMember(String name, String gender, int age, String familyRole) {
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.familyRole = familyRole;
    }

    public void getInformation(){
        System.out.println("Имя: "+name+". Пол: "+gender+". Возраст: "+age+". Роль в семье: "+familyRole+".");
    }

}
